# git-pull-loop-script.sh   

## Description
The script loops inside a hierarchy of folders containing Git projects and updates them with the `git pull` command. It also creates a log file in its working directory, compiling both stdout and stderr.

Please read the script itself for more details.

## How-to
The script has to be run in the folder which contains the Git projects you want to pull:
```
$ cd my-git-projects
$ chmod +x git-pull-loop-script.sh
$ ./path/to/git-pull-loop-script.sh
```
Read the logs:

```
$ less git-pull-loop-script.log
```