#!/bin/bash

#To be run in the folder which contains the Git projects you want to pull.

#exec &> git-pull-loop-script.log #Terminal output log

{ #Braces surrounding the script are used to collect stdout and stderr of the whole script (see at the end)

	SAVEIFS=$IFS # $IFS variable backup (Internal field separator)
	IFS=$(echo -en "\n\b") #Changes the $IFS variable in order to loop inside folders whose names contain spaces
	DIR=$(printf "%q\n" "$(pwd)")

	#for i in "$(ls -b)"
	#for i in ./*/
	for i in $(find . -maxdepth 1 -mindepth 1 -type d -printf '%f\n')
	do
		#echo "$i"
		cd "$i"

		if [ -e .git ]
		then
			pwd
			git pull
		fi

		cd $DIR
	done

	IFS=$SAVEIFS #Restores $IFS variable

} 2>&1 | tee git-pull-loop-script.log #Send both stdout and stderr in a log filelocated in the script working directory
